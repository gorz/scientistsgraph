package com.scientists.graph.algorithm;

import com.scientists.graph.graph.GraphBase;
import org.graphstream.algorithm.Prim;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;

/**
 * Created by vhapiak on 13.03.2016.
 */
public class SpanningTree extends ConnectedComponents {

    public SpanningTree(double threshold) {
        super(threshold);
    }

    public static final String INV_WEIGHT_ATTR = "invWeight";

    @Override
    public void clusterize(GraphBase g) {
        Graph graph = g.getGraph();
        for(Edge edge : graph.getEachEdge()) {
            edge.setAttribute(INV_WEIGHT_ATTR, 1 / edge.getNumber("weight"));
        }

        Prim prim = new Prim(INV_WEIGHT_ATTR, "spanningTree", "true", "false");
        prim.init(graph);
        prim.compute();

        super.clusterize(g);
    }

    @Override
    protected boolean isUnnecessaryEdge(Edge edge) {
        return super.isUnnecessaryEdge(edge)|| edge.getAttribute("spanningTree").equals("false");
    }
}
