package com.scientists.graph.algorithm;

import org.graphstream.graph.Node;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 13.03.2016.
 */
public class Cluster implements Iterable<Node> {

    private List<Node> mNodes = new LinkedList<Node>();

    public void addNode(Node node) {
        mNodes.add(node);
    }

    List<Node> getList()
    {
        return mNodes;
    }

    @Override
    public Iterator<Node> iterator() {
        return mNodes.iterator();
    }
}
