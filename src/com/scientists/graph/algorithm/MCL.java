package com.scientists.graph.algorithm;

import com.scientists.graph.graph.GraphBase;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealMatrixChangingVisitor;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;

/**
 * Created by vhapiak on 19.10.2016.
 */
public class MCL extends Algorithm {

    private double mR = 0;
    private int mE = 0;

    public MCL(double r)
    {
        this(r, 2);
    }

    public MCL(double r, int e) {
        mR = r;
        mE = e;
    }

    @Override
    public void clusterize(GraphBase g) {
        Graph graph = g.getGraph();

        int nodes = graph.getNodeCount();
        int edges = graph.getEdgeCount();
        RealMatrix stochasticMatrix = new Array2DRowRealMatrix(nodes, nodes);

        // build adjacency matrix
        double sum = 0;
        for(Edge e : graph.getEachEdge()) {
            double w = e.getNumber("weight");
            sum += w;
            int node0 = e.getNode0().getIndex();
            int node1 = e.getNode1().getIndex();
            stochasticMatrix.setEntry(node0, node1, w);
            stochasticMatrix.setEntry(node1, node0, w);
        }

        // add self loops
        double avg = sum / edges;
        for (int i = 0; i < nodes; i++)
        {
            stochasticMatrix.setEntry(i, i, avg);
        }

        normalize(stochasticMatrix);

        double diff = 0;
        RealMatrix M = stochasticMatrix.copy();
        do {
            RealMatrix expand = M.power(mE);

            expand.walkInOptimizedOrder(new RealMatrixChangingVisitor() {
                @Override
                public void start(int i, int i1, int i2, int i3, int i4, int i5) {

                }

                @Override
                public double visit(int i, int i1, double v) {
                    return Math.pow(v, mR);
                }

                @Override
                public double end() {
                    return 0;
                }
            });

            normalize(expand);
            RealMatrix sub = M.subtract(expand);
            diff = sub.getFrobeniusNorm();
            M = expand;
//            System.out.println("Iteration, diff: " + diff);
        } while(diff >= 0.01);

        Cluster clusters[] = new Cluster[nodes];
        for (int i = 0; i < M.getColumnDimension(); i++)
        {
            double[] column = M.getColumn(i);
            double max = column[0];
            int imax = 0;
            for (int j = 1; j < column.length; j++)
            {
                if (column[j] > max)
                {
                    max = column[j];
                    imax = j;
                }
            }
            Cluster cluster = clusters[imax];
            if (cluster == null)
            {
                cluster = new Cluster();
                clusters[imax] = cluster;
            }
            cluster.addNode(graph.getNode(i));
        }

        for (Cluster cluster : clusters)
        {
            if (cluster != null)
            {
                g.addCluster(cluster);
            }
        }
    }

    private void normalize(RealMatrix M)
    {
        // normalize by columns
        for (int i = 0; i < M.getRowDimension(); i++)
        {
            double colSum = 0;
            double[] column = M.getColumn(i);
            for (int j = 0; j < M.getColumnDimension(); j++)
            {
                colSum += column[j];
            }
            for (int j = 0; j < M.getColumnDimension(); j++)
            {
                column[j] /= colSum;
            }
            M.setColumn(i, column);
        }
    }
}
