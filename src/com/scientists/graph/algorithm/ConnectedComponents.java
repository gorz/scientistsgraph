package com.scientists.graph.algorithm;

import com.scientists.graph.graph.GraphBase;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 11.03.2016.
 */
public class ConnectedComponents extends Algorithm {

    protected double mThreshold = 0;

    public ConnectedComponents(double threshold) {
        mThreshold = threshold;
    }

    @Override
    public void clusterize(GraphBase g) {
        Graph graph = g.getGraph();

        List<Edge> removedEdges = new LinkedList<>();

        removeEdges(graph, removedEdges);

        org.graphstream.algorithm.ConnectedComponents connectedComponents = new org.graphstream.algorithm.ConnectedComponents();
        connectedComponents.init(graph);
        connectedComponents.setCountAttribute("cluster");
        connectedComponents.compute();

        for(org.graphstream.algorithm.ConnectedComponents.ConnectedComponent component : connectedComponents) {
            Cluster cluster = new Cluster();
            for(Node node : component) {
                cluster.addNode(node);
            }
            g.addCluster(cluster);
        }

        connectedComponents.terminate();
        restoreEdges(graph, removedEdges);
    }

    private void removeEdges(Graph graph, List<Edge> edges) {
        for(Edge e : graph.getEachEdge()) {
            if(isUnnecessaryEdge(e)) {
                edges.add(e);
            }
        }
        for(Edge e : edges) {
            Edge edge = graph.removeEdge(e);
        }
    }

    private void restoreEdges(Graph graph, List<Edge> edges) {
        for(Edge e : edges) {
            Edge edge = graph.addEdge(e.getId(), e.<Node>getNode0(), e.getNode1());
            edge.addAttribute("ui.label", e.getNumber("ui.label"));
            edge.addAttribute("weight", e.getNumber("weight"));
        }
    }

    protected boolean isUnnecessaryEdge(Edge edge) {
        return edge.getNumber("weight") < mThreshold;
    }
}
