package com.scientists.graph.algorithm;

import com.scientists.graph.graph.GraphBase;

/**
 * Created by vhapiak on 11.03.2016.
 */
public abstract class Algorithm {

    public abstract void clusterize(GraphBase graph);

}

