package com.scientists.graph.data;

import java.sql.*;
import java.util.HashMap;

/**
 * Created by vhapiak on 13.11.2016.
 */
public class DataManager {

    private HashMap<Integer, Article> mArticles = new HashMap<>();
    private HashMap<String, Author> mAuthors = new HashMap<>();
    private HashMap<String, KeyWord> mKeyWords = new HashMap<>();

    public  DataManager()
    {

    }

    public DataManager(String filename) throws SQLException
    {
        Connection connection = DriverManager.getConnection("jdbc:sqlite:" + filename);

        Statement statement = connection.createStatement();

        ResultSet result = statement.executeQuery("SELECT * FROM article;");
        while(result.next())
        {
            int id = result.getInt("id");
            String name = result.getString("name");

            mArticles.put(id, new Article(id, name));
        }
        result.close();

        result = statement.executeQuery(
                "SELECT id, name, article_id FROM author A JOIN author_articles AA ON A.id == AA.author_id ORDER BY id;");
        int authorId = -1;
        Author author = null;
        while(result.next())
        {
            int id = result.getInt("id");
            String name = result.getString("name");
            int articleId = result.getInt("article_id");

            if (id != authorId)
            {
                author = new Author(id, name);
                authorId = id;
                mAuthors.put(name, author);
            }

            Article article = mArticles.get(articleId);
            article.addAuthor(author);
            author.addArticle(article);
        }
        result.close();

        result = statement.executeQuery(
                "SELECT id, word, article_id FROM key_word K JOIN article_key_words KA ON K.id == KA.key_word_id ORDER BY id;");
        int wordId = -1;
        KeyWord keyWord = null;
        while(result.next())
        {
            int id = result.getInt("id");
            String word = result.getString("word");
            int articleId = result.getInt("article_id");

            if (id != wordId)
            {
                keyWord = new KeyWord(id, word);
                wordId = id;
                mKeyWords.put(word, keyWord);
            }

            Article article = mArticles.get(articleId);
            article.addKeyWord(keyWord);
            keyWord.addArticle(article);
        }
        result.close();

        statement.close();
        connection.close();
    }

    public HashMap<String, Author> getAuthors()
    {
        return mAuthors;
    }

    public HashMap<String, KeyWord> getKeyWords()
    {
        return mKeyWords;
    }
}
