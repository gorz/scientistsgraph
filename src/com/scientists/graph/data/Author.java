package com.scientists.graph.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vhapiak on 13.11.2016.
 */
public class Author {

    private int mId;
    private String mName;
    private List<Article> mArticles;

    public Author(int id, String name)
    {
        mId = id;
        mName = name;
        mArticles = new ArrayList<>();
    }

    void addArticle(Article article)
    {
        mArticles.add(article);
    }

    public int getId()
    {
        return mId;
    }

    public String getName()
    {
        return  mName;
    }

    public List<Article> getArticles()
    {
        return mArticles;
    }
}
