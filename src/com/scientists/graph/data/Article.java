package com.scientists.graph.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vhapiak on 13.11.2016.
 */
public class Article {

    private int mId;
    private String mName;
    private List<Author> mAuthors;
    private List<KeyWord> mKeyWords;

    public Article(int id, String name)
    {
        mId = id;
        mName = name;
        mAuthors = new ArrayList<>();
        mKeyWords = new ArrayList<>();
    }

    void addAuthor(Author author)
    {
        mAuthors.add(author);
    }

    void addKeyWord(KeyWord word)
    {
        mKeyWords.add(word);
    }

    public int getId()
    {
        return mId;
    }

    public String getName()
    {
        return  mName;
    }

    public List<Author> getAuthors()
    {
        return mAuthors;
    }

    public List<KeyWord> getKeyWords()
    {
        return mKeyWords;
    }
}
