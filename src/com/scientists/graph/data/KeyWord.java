package com.scientists.graph.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vhapiak on 13.11.2016.
 */
public class KeyWord {

    private int mId;
    private String mWord;
    private List<Article> mArticles;

    public KeyWord(int id, String word)
    {
        mId = id;
        mWord = word;
        mArticles = new ArrayList<>();
    }

    void addArticle(Article article)
    {
        mArticles.add(article);
    }

    public int getId()
    {
        return mId;
    }

    public String getWord()
    {
        return  mWord;
    }

    public List<Article> getArticles()
    {
        return mArticles;
    }
}
