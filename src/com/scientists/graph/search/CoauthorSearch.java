package com.scientists.graph.search;

import com.scientists.graph.data.Author;
import com.scientists.graph.data.DataManager;
import com.scientists.graph.graph.CoauthorGraph;
import com.scientists.graph.graph.Group;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 19.11.2016.
 */
public class CoauthorSearch {

    public class Result {
        public Author mAuthor;
        public int mCount;

        public Result(Author author, int count)
        {
            mAuthor = author;
            mCount = count;
        }
    }

    CoauthorGraph mGraph;
    DataManager mData;

    public CoauthorSearch(DataManager data, CoauthorGraph graph)
    {
        mData = data;
        mGraph = graph;
    }

    public List<Result> search(String name)
    {
        List<Result> result = new LinkedList<>();
        Group<Author> group = mGraph.getGroup(name);

        if (group == null) throw new RuntimeException();

        Author author = mData.getAuthors().get(name);
        for (Author a : group)
        {
            if (a == author) continue;
            int count = mGraph.getEdgeWeight(author, a);
            if (count != 0)
                result.add(new Result(a, count));
        }
        return result;
    }

}
