package com.scientists.graph.search;

import com.scientists.graph.data.Article;
import com.scientists.graph.data.Author;
import com.scientists.graph.data.DataManager;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.Group;
import com.scientists.graph.graph.SemanticGraph;
import scala.Int;

import java.security.Key;
import java.util.*;

/**
 * Created by vhapiak on 19.11.2016.
 */
public class DirectionsSearch {

    public class Result {
        public Group<KeyWord> mGroup;
        public int mTotalUsages;

        public Result (Group<KeyWord> group, int totalUsages)
        {
            mGroup = group;
            mTotalUsages = totalUsages;
        }
    }

    SemanticGraph mGraph;
    DataManager mData;

    public DirectionsSearch(DataManager data, SemanticGraph graph)
    {
        mData = data;
        mGraph = graph;
    }

    public List<Result> search(String name)
    {
        List<Result> results = new LinkedList<>();

        Author author = mData.getAuthors().get(name);
        if (author == null) throw new RuntimeException();

        HashMap<KeyWord, Integer> usedKeyWords = new HashMap<>();
        HashSet<Group<KeyWord>> groups = new HashSet<>();
        for (Article article : author.getArticles())
        {
            for (KeyWord word : article.getKeyWords())
            {
                Integer i = usedKeyWords.get(word);
                if (i == null)
                {
                    i = 0;
                }
                usedKeyWords.put(word, ++i);
                groups.add(mGraph.getGroup(word.getWord()));
            }
        }

        for (Group<KeyWord> group : groups)
        {
            int usages = 0;
            for (KeyWord word : group)
            {
                Integer i = usedKeyWords.get(word);
                usages += i == null ? 0 : i;
            }
            if (usages != 0)
            {
                results.add(new Result(group, usages));
            }
        }

        return results;
    }
}
