package com.scientists.graph.search;

import com.scientists.graph.data.Article;
import com.scientists.graph.data.Author;
import com.scientists.graph.data.DataManager;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.CoauthorGraph;
import com.scientists.graph.graph.Group;
import com.scientists.graph.graph.SemanticGraph;

import java.util.*;

/**
 * Created by vhapiak on 19.11.2016.
 */
public class AuthorsSearch {

    public class Result {
        public Author mAuthor;
        public int mCount;

        public Result(Author author, int count)
        {
            mAuthor = author;
            mCount = count;
        }
    }

    SemanticGraph mGraph;
    DataManager mData;

    public AuthorsSearch(DataManager data, SemanticGraph graph)
    {
        mData = data;
        mGraph = graph;
    }

    public List<Result> search(List<String> words)
    {
        List<Result> result = new LinkedList<>();

        HashSet<KeyWord> keyWords = new HashSet<>();
        for (String word : words)
        {
            Group<KeyWord> group = mGraph.getGroup(word);
            if (group == null) continue;
            for (KeyWord w : group)
            {
                keyWords.add(w);
            }
        }

        HashMap<Author, Integer> authors = new HashMap<>();
        for (KeyWord w : keyWords)
        {
            for (Article article : w.getArticles())
            {
                for (Author author : article.getAuthors())
                {
                    Integer i = authors.get(author);
                    if (i == null)
                    {
                        i = 0;
                    }
                    authors.put(author, ++i);
                }
            }
        }

        for (Map.Entry<Author, Integer> e : authors.entrySet())
        {
            if (e.getValue() != 0)
                result.add(new Result(e.getKey(), e.getValue()));
        }

        return result;
    }
}
