package com.scientists.graph.search;

import com.scientists.graph.data.Article;
import com.scientists.graph.data.Author;
import com.scientists.graph.data.DataManager;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.Group;
import com.scientists.graph.graph.SemanticGraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 19.11.2016.
 */
public class NewRelationsSearch {

    SemanticGraph mGraph;
    DataManager mData;

    public NewRelationsSearch(DataManager data, SemanticGraph graph)
    {
        mData = data;
        mGraph = graph;
    }

    public List<Author> search(String name)
    {
        List<Author> results = new LinkedList<>();

        Author author = mData.getAuthors().get(name);
        if (author == null) throw new RuntimeException();

        HashSet<Group<KeyWord>> groups = new HashSet<>();
        HashSet<Author> coauthors = new HashSet<>();
        for (Article article : author.getArticles())
        {
            coauthors.addAll(article.getAuthors());
            for (KeyWord word : article.getKeyWords())
            {
                groups.add(mGraph.getGroup(word.getWord()));
            }
        }

        for (Group<KeyWord> group : groups)
        {
            for (KeyWord word : group)
            {
                for (Article article : word.getArticles())
                {
                    for (Author a : article.getAuthors())
                    {
                        if (!coauthors.contains(a))
                        {
                            coauthors.add(a);
                            results.add(a);
                        }
                    }
                }

            }
        }

        return results;
    }
}
