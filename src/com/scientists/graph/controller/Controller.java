package com.scientists.graph.controller;

import com.scientists.graph.Model;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class Controller {

    protected Stage mStage;
    protected Model mModel;


    @FXML
    protected VBox layout;

    public void init(Stage stage, Model model)
    {
        mStage = stage;
        mModel = model;
    }

}
