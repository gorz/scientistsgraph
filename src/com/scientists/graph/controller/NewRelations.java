package com.scientists.graph.controller;

import com.scientists.graph.Model;
import com.scientists.graph.data.Author;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class NewRelations extends SearchByAuthor {


    @FXML
    private ListView<String> list;

    @FXML
    private void doAction()
    {
        List<Author> results = new LinkedList<>();
        try
        {
            results = mModel.searchNewRelations(search.getText());
        } catch (RuntimeException e)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Повідомлення");
            alert.setHeaderText("Не знайдено заданого автора!");
            alert.showAndWait();
        }
        list.getItems().clear();
        for (Author author : results)
        {
            list.getItems().add(author.getName());
        }
    }

    @Override
    public void init(Stage stage, Model model) {
        super.init(stage, model);

        list.prefHeightProperty().bind(layout.heightProperty());
    }
}
