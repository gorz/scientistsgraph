package com.scientists.graph.controller;

import com.scientists.graph.Model;
import com.scientists.graph.search.CoauthorSearch;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class Coauthors extends SearchByAuthor {

    public class Result {
        private final SimpleStringProperty name = new SimpleStringProperty("");
        private final SimpleIntegerProperty count = new SimpleIntegerProperty(0);

        public Result(CoauthorSearch.Result result)
        {
            setName(result.mAuthor.getName());
            setCount(result.mCount);
        }

        public String getName()
        {
            return name.get();
        }

        public void setName(String fName)
        {
            name.set(fName);
        }

        public int getCount()
        {
            return count.get();
        }

        public void setCount(int fCount)
        {
            count.set(fCount);
        }
    }

    @FXML
    TableView<Result> table;

    @FXML
    TableColumn column;

    @FXML
    private void doAction()
    {
        List<CoauthorSearch.Result> results = new LinkedList<>();
        try
        {
            results = mModel.searchCoauthors(search.getText());
        } catch (RuntimeException e)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Повідомлення");
            alert.setHeaderText("Не знайдено заданого автора!");
            alert.showAndWait();
        }
        Collections.sort(results, new Comparator<CoauthorSearch.Result>() {
            @Override
            public int compare(CoauthorSearch.Result f, CoauthorSearch.Result s) {
                return s.mCount - f.mCount;
            }
        });
        table.getItems().clear();
        for (CoauthorSearch.Result result : results)
        {
            table.getItems().add(new Result(result));
        }
    }

    @Override
    public void init(Stage stage, Model model) {
        super.init(stage, model);

        table.prefHeightProperty().bind(layout.heightProperty());
        column.prefWidthProperty().bind(table.widthProperty().subtract(148));
    }
}
