package com.scientists.graph.controller;

import com.scientists.graph.data.Author;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.Group;
import com.scientists.graph.view.MainView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;

import java.io.IOException;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class KeyWordsCell {
    @FXML
    private FlowPane pane;

    public KeyWordsCell(Group<KeyWord> items) throws IOException {
        FXMLLoader loader = new FXMLLoader(MainView.class.getResource("cell.fxml"));
        loader.setController(this);
        loader.load();

        for (KeyWord item : items)
        {
            Label label = new Label(item.getWord() + ";");
            pane.getChildren().add(label);
        }
    }

    public FlowPane getPane()
    {
        return pane;
    }
}
