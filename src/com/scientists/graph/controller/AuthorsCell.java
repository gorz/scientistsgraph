package com.scientists.graph.controller;

import com.scientists.graph.data.Author;
import com.scientists.graph.graph.Group;
import com.scientists.graph.view.MainView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

import java.io.IOException;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class AuthorsCell {
    @FXML
    private FlowPane pane;

    public AuthorsCell(Group<Author> items) throws IOException {
        FXMLLoader loader = new FXMLLoader(MainView.class.getResource("cell.fxml"));
        loader.setController(this);
        loader.load();

        for (Author item : items)
        {
            Label label = new Label(item.getName() + ";");
            pane.getChildren().add(label);
        }
    }

    public AuthorsCell() throws IOException {
        FXMLLoader loader = new FXMLLoader(MainView.class.getResource("cell.fxml"));
        loader.setController(this);
        loader.load();
    }

    public FlowPane getPane()
    {
        return pane;
    }
}
