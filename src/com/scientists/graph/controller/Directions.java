package com.scientists.graph.controller;

import com.scientists.graph.Model;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.Group;
import com.scientists.graph.search.CoauthorSearch;
import com.scientists.graph.search.DirectionsSearch;
import com.scientists.graph.view.KeyWordsTableCellView;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class Directions extends SearchByAuthor {

    public class Result {
        private final SimpleObjectProperty<Group<KeyWord>> group = new SimpleObjectProperty<>();
        private final SimpleIntegerProperty count = new SimpleIntegerProperty(0);

        public Result(DirectionsSearch.Result result)
        {
            setGroup(result.mGroup);
            setCount(result.mTotalUsages);
        }

        public Group<KeyWord> getGroup()
        {
            return group.get();
        }

        public void setGroup(Group<KeyWord> fGroup)
        {
            group.set(fGroup);
        }

        public int getCount()
        {
            return count.get();
        }

        public void setCount(int fCount)
        {
            count.set(fCount);
        }
    }

    @FXML
    private TableView<Result> table;

    @FXML
    private TableColumn column;

    @FXML
    private void doAction()
    {
        List<DirectionsSearch.Result> results = new LinkedList<>();
        try
        {
            results = mModel.searchDirections(search.getText());
        } catch (RuntimeException e)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Повідомлення");
            alert.setHeaderText("Не знайдено заданого автора!");
            alert.showAndWait();
        }
        Collections.sort(results, new Comparator<DirectionsSearch.Result>() {
            @Override
            public int compare(DirectionsSearch.Result f, DirectionsSearch.Result s) {
                return s.mTotalUsages - f.mTotalUsages;
            }
        });

        table.getItems().clear();
        for (DirectionsSearch.Result result : results)
        {
            table.getItems().add(new Result(result));
        }
    }

    @Override
    public void init(Stage stage, Model model) {
        super.init(stage, model);

        TableColumn<Result, Group<KeyWord>> column = (TableColumn<Result, Group<KeyWord>>) table.getColumns().get(0);
        column.setCellFactory(new Callback<TableColumn<Result, Group<KeyWord>>, TableCell<Result, Group<KeyWord>>>() {
            @Override
            public TableCell<Result, Group<KeyWord>> call(TableColumn<Result, Group<KeyWord>> param) {
                return new KeyWordsTableCellView();
            }
        });

        table.prefHeightProperty().bind(layout.heightProperty());
        this.column.prefWidthProperty().bind(table.widthProperty().subtract(148));
    }
}
