package com.scientists.graph.controller;

import com.scientists.graph.Model;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.Group;
import com.scientists.graph.view.KeyWordsListCellView;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class ResearchDirections extends Controller implements Callback<ListView<Group<KeyWord>>, ListCell<Group<KeyWord>>> {

    @FXML
    private ListView<Group<KeyWord>> list;

    @Override
    public void init(Stage stage, Model model) {
        super.init(stage, model);

        list.setCellFactory(this);
        list.getItems().addAll(mModel.getKeyWordsGroups());

        list.prefHeightProperty().bind(layout.heightProperty());
    }

    @Override
    public ListCell<Group<KeyWord>> call(ListView<Group<KeyWord>> param) {
        return new KeyWordsListCellView();
    }
}
