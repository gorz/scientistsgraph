package com.scientists.graph.controller;

import com.scientists.graph.Model;
import com.scientists.graph.search.AuthorsSearch;
import com.scientists.graph.search.CoauthorSearch;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.util.*;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class Authors extends Controller implements EventHandler<MouseEvent> {

    public class Result {
        private final SimpleStringProperty name = new SimpleStringProperty("");
        private final SimpleIntegerProperty count = new SimpleIntegerProperty(0);

        public Result(AuthorsSearch.Result result)
        {
            setName(result.mAuthor.getName());
            setCount(result.mCount);
        }

        public String getName()
        {
            return name.get();
        }

        public void setName(String fName)
        {
            name.set(fName);
        }

        public int getCount()
        {
            return count.get();
        }

        public void setCount(int fCount)
        {
            count.set(fCount);
        }
    }

    @FXML
    TableView<Result> table;

    @FXML
    protected TextField search;

    @FXML
    protected FlowPane words;

    @FXML
    protected TableColumn column;

    private HashSet<String> mKeyWords = new HashSet<>();

    @FXML
    private void doAction()
    {
        List<String> list = new LinkedList<>(mKeyWords);
        List<AuthorsSearch.Result> results = mModel.searchAuthors(list);
        Collections.sort(results, new Comparator<AuthorsSearch.Result>() {
            @Override
            public int compare(AuthorsSearch.Result f, AuthorsSearch.Result s) {
                return s.mCount - f.mCount;
            }
        });
        table.getItems().clear();
        for (AuthorsSearch.Result result : results)
        {
            table.getItems().add(new Result(result));
        }
    }

    @FXML
    private void addKeyWord()
    {
        if (!mModel.getData().getKeyWords().containsKey(search.getText()))
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Повідомлення");
            alert.setHeaderText("Не знайдено заданого ключового слова!");
            alert.showAndWait();
            return;
        }
        if(mKeyWords.add(search.getText()))
        {
            try {
                KeyWordItem item = new KeyWordItem(search.getText(), this);
                words.getChildren().add(item.getBox());
            } catch (IOException e) {
                throw new RuntimeException("Cannot parse fxml", e);
            }
        }
    }

    @Override
    public void init(Stage stage, Model model) {
        super.init(stage, model);
        TextFields.bindAutoCompletion(search, model.getData().getKeyWords().keySet());

        table.prefHeightProperty().bind(layout.heightProperty());
        column.prefWidthProperty().bind(layout.widthProperty().subtract(148));
    }


    @Override
    public void handle(MouseEvent event) {
        ImageView button = (ImageView) event.getSource();
        String keyWord = (String) button.getUserData();
        mKeyWords.remove(keyWord);
        words.getChildren().remove(button.getParent().getParent());
    }
}
