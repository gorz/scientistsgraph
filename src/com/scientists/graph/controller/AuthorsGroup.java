package com.scientists.graph.controller;

import com.scientists.graph.Model;
import com.scientists.graph.data.Author;
import com.scientists.graph.graph.Group;
import com.scientists.graph.view.AuthorsCellView;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class AuthorsGroup extends Controller implements Callback<ListView<Group<Author>>, ListCell<Group<Author>>> {

    @FXML
    private ListView<Group<Author>> list;

    @Override
    public void init(Stage stage, Model model) {
        super.init(stage, model);

        list.setCellFactory(this);
        list.getItems().addAll(mModel.getAuthorsGroups());

        list.prefHeightProperty().bind(layout.heightProperty());
    }

    @Override
    public ListCell<Group<Author>> call(ListView<Group<Author>> param) {
        return new AuthorsCellView();
    }
}
