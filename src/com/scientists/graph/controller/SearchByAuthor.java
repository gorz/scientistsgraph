package com.scientists.graph.controller;

import com.scientists.graph.Model;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class SearchByAuthor extends Controller {

    @FXML
    protected TextField search;

    @Override
    public void init(Stage stage, Model model) {
        super.init(stage, model);

        TextFields.bindAutoCompletion(search, model.getData().getAuthors().keySet());
    }
}
