package com.scientists.graph.controller;

import com.scientists.graph.Model;
import com.scientists.graph.view.MainView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class Menu extends Controller {

    @FXML
    private void showAuthorsGroups()
    {
        open("authors_groups.fxml", 600, 600);
    }

    @FXML
    private void showResearchDirections()
    {
        open("research_directions.fxml", 600, 600);
    }

    @FXML
    private void searchNewRelations()
    {
        open("new_relations.fxml", 600, 600);
    }

    @FXML
    private void searchCoauthors()
    {
        open("coauthors.fxml", 600, 600);
    }

    @FXML
    private void searchDirections()
    {
        open("directions.fxml", 600, 600);
    }

    @FXML
    private void searchAuthors()
    {
        open("authors.fxml", 600, 600);
    }

    @FXML
    private void changeFile()
    {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("SQLite files", "*.db"));
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File file = chooser.showOpenDialog(mStage);
        if (file != null)
        {
            try {
                mModel.selectFile(file.getAbsolutePath());
            } catch (SQLException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Помилка");
                alert.setHeaderText("Неможливо завантажити дані з обраного файлу! Оберіть інший файл.");
                alert.showAndWait();
                e.printStackTrace();
            }
        }
    }

    private void open(String fxml, int width, int height)
    {
        FXMLLoader loader = new FXMLLoader(MainView.class.getResource(fxml));
        try {
            Parent root = loader.load();
            Stage stage = new Stage();
            Controller controller = loader.getController();
            controller.init(stage, mModel);
            stage.setTitle("Система аналізу публікацій");
            stage.setScene(new Scene(root, width, height));
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException("Cannot parse fxml", e);
        }
    }

    @Override
    public void init(Stage stage, Model model) {
        super.init(stage, model);
    }
}
