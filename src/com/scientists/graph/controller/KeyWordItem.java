package com.scientists.graph.controller;

import com.scientists.graph.data.Author;
import com.scientists.graph.graph.Group;
import com.scientists.graph.view.MainView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;

import java.io.IOException;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class KeyWordItem {
    @FXML
    private Label word;

    @FXML
    private ImageView remove;

    @FXML
    private HBox box;

    public KeyWordItem(String keyWord, EventHandler<MouseEvent> handler) throws IOException {
        FXMLLoader loader = new FXMLLoader(MainView.class.getResource("keyWord.fxml"));
        loader.setController(this);
        loader.load();

        word.setText(keyWord);
        remove.setUserData(keyWord);
        remove.setOnMouseClicked(handler);
    }

    public HBox getBox()
    {
        return box;
    }
}
