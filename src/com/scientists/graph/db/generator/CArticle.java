package com.scientists.graph.db.generator;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 * Created by vhapiak on 29.02.2016.
 */
public class CArticle {

    private static int ID = 0;

    private int mId;
    private String mName;
    private HashSet<CKeyWord> mKeyWords;
    private HashSet<CArticle> mLinks;

    public CArticle() {
        mId = ID++;
        mName = "Author" + mId;
        mKeyWords = new HashSet<>();
        mLinks = new HashSet<>();
    }

    public CArticle(CAuthor authors[], CKeyWord keyWords[]) {
        mId = ID++;
        mKeyWords = new HashSet<>();
        mLinks = new HashSet<>();
        mName = "";
        for(CAuthor author : authors) {
            mName += author.getName();
            author.addArticle(this);
        }
        for(CKeyWord keyWord : keyWords) {
            mKeyWords.add(keyWord);
        }
        mName += mId;
    }

    public void addKeyWord(CKeyWord word) {
        mKeyWords.add(word);
    }

    public void addLink(CArticle article) {
        mLinks.add(article);
    }

    public void saveToArticle(Statement statement) throws SQLException {
        statement.addBatch(String.format(
                "INSERT INTO article \n" +
                "VALUES (%d, '%s');", mId, mName));
    }

    public void saveToArticleKeyWords(Statement statement) throws SQLException {
        for(CKeyWord keyWord : mKeyWords) {
            statement.addBatch(String.format(
                    "INSERT INTO article_key_words \n" +
                    "VALUES (%d, %d);", mId, keyWord.getId()));
        }
    }

    public void saveToArticleLinks(Statement statement) throws SQLException {
        for(CArticle article : mLinks) {
            statement.addBatch(String.format(
                    "INSERT INTO article_links \n" +
                    "VALUES (%d, %d);", mId, article.getId()));
        }
    }

    public int getId() {
        return mId;
    }
}
