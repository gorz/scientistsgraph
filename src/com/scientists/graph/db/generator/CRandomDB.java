package com.scientists.graph.db.generator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.*;
import java.util.*;

/**
 * Created by vhapiak on 29.02.2016.
 */
public class CRandomDB {


    private Document mConfig;
    private Connection mDBConnection;
    private ArrayList<CAuthor> mAuthors;
    private ArrayList<CKeyWord> mKeyWords;
    private LinkedList<CArticle> mArticles;
    private Random mRandom;

    static {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public CRandomDB(String dbPath, Document config) throws SQLException {
        mConfig = config;
        mArticles = new LinkedList<>();
        mRandom = new Random();

        mDBConnection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
        mDBConnection.setAutoCommit(false);
        createSchema();
//
//        int authorsNumber = getChildAttrValue("authors", "number");
//        mAuthors = new CAuthor[authorsNumber];
//
//        int keyWordsNumber = getChildAttrValue("keyWords", "number");
//        mKeyWords = new CKeyWord[keyWordsNumber];
//
//        generate();

        CAuthor A = new CAuthor("Кавицкая");
        CAuthor B = new CAuthor("Любченко");
        CAuthor C = new CAuthor("Крисилов");
        CAuthor D = new CAuthor("Кунгурцев");
        CAuthor E = new CAuthor("Тройнина");
        CAuthor F = new CAuthor("Рувинская");
        CAuthor G = new CAuthor("Зиноватная");

        mAuthors = new ArrayList<CAuthor>();
        mAuthors.add(A);
        mAuthors.add(B);
        mAuthors.add(C);
        mAuthors.add(D);
        mAuthors.add(E);
        mAuthors.add(F);
        mAuthors.add(G);

        CKeyWord H = new CKeyWord("Big Data");
        CKeyWord I = new CKeyWord("AI");
        CKeyWord J = new CKeyWord("Key Word");
        CKeyWord K = new CKeyWord("JIT");
        CKeyWord L = new CKeyWord("NoSQL");
        CKeyWord M = new CKeyWord("DB Analyzer");
        CKeyWord N = new CKeyWord("SQL");
        CKeyWord O = new CKeyWord("CIL");
        CKeyWord P = new CKeyWord("Assembler");

        mKeyWords = new ArrayList<CKeyWord>();
        mKeyWords.add(H);
        mKeyWords.add(I);
        mKeyWords.add(J);
        mKeyWords.add(K);
        mKeyWords.add(L);
        mKeyWords.add(M);
        mKeyWords.add(N);
        mKeyWords.add(O);
        mKeyWords.add(P);

        mArticles.add(new CArticle(new CAuthor[]{C, B, A}, new CKeyWord[]{H, J}));
        mArticles.add(new CArticle(new CAuthor[]{C, B, A}, new CKeyWord[]{H}));
        mArticles.add(new CArticle(new CAuthor[]{B, A}, new CKeyWord[]{H, J}));
        mArticles.add(new CArticle(new CAuthor[]{C, A}, new CKeyWord[]{I}));
        mArticles.add(new CArticle(new CAuthor[]{C, A}, new CKeyWord[]{I}));
        mArticles.add(new CArticle(new CAuthor[]{C, A}, new CKeyWord[]{I}));
        mArticles.add(new CArticle(new CAuthor[]{B, C}, new CKeyWord[]{I, J}));
        mArticles.add(new CArticle(new CAuthor[]{B, C}, new CKeyWord[]{I, J}));
        mArticles.add(new CArticle(new CAuthor[]{B, C}, new CKeyWord[]{I, J}));
        mArticles.add(new CArticle(new CAuthor[]{B, C}, new CKeyWord[]{I, J}));
        mArticles.add(new CArticle(new CAuthor[]{B, C}, new CKeyWord[]{I, J}));
        mArticles.add(new CArticle(new CAuthor[]{B, C}, new CKeyWord[]{J}));
        mArticles.add(new CArticle(new CAuthor[]{B, C}, new CKeyWord[]{J}));
        mArticles.add(new CArticle(new CAuthor[]{B, C}, new CKeyWord[]{J}));
        mArticles.add(new CArticle(new CAuthor[]{B, C, D}, new CKeyWord[]{L, J}));
        mArticles.add(new CArticle(new CAuthor[]{C, D}, new CKeyWord[]{L}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{L}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{L}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{M, N}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{M, N}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{M}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{N}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{N}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{N}));
        mArticles.add(new CArticle(new CAuthor[]{D, G}, new CKeyWord[]{N}));
        mArticles.add(new CArticle(new CAuthor[]{D, E}, new CKeyWord[]{N}));
        mArticles.add(new CArticle(new CAuthor[]{D, E}, new CKeyWord[]{N}));
        mArticles.add(new CArticle(new CAuthor[]{F, E}, new CKeyWord[]{K, O}));
        mArticles.add(new CArticle(new CAuthor[]{F, E}, new CKeyWord[]{K}));
        mArticles.add(new CArticle(new CAuthor[]{F, E}, new CKeyWord[]{P}));
        mArticles.add(new CArticle(new CAuthor[]{F, E}, new CKeyWord[]{P}));
        mArticles.add(new CArticle(new CAuthor[]{F, E}, new CKeyWord[]{P}));
        mArticles.add(new CArticle(new CAuthor[]{F, E}, new CKeyWord[]{P}));
        mArticles.add(new CArticle(new CAuthor[]{F, E}, new CKeyWord[]{P}));
        saveToDB();
    }

    public static void main(String[] args) throws SQLException, ParserConfigurationException, IOException, SAXException {
        if(args.length < 2) {
            System.out.println("Args: db.db config.xml");
            return;
        }
        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        new CRandomDB(args[0], docBuilder.parse(args[1]));
    }

    private void generate() {
//        for(int i = 0; i < mAuthors.length; i++) {
//            mAuthors[i] = new CAuthor();
//        }
//
//        for(int i = 0; i < mKeyWords.length; i++) {
//            mKeyWords[i] = new CKeyWord();
//        }
//
//        int minCoauthors = getChildAttrValue("coauthors", "min");
//        int maxCoauthors = getChildAttrValue("coauthors", "max");
//        int minArticlesPerAuthor = getChildAttrValue("articlesPerAuthor", "min");
//        int maxArticlesPerAuthor = getChildAttrValue("articlesPerAuthor", "max");
//        for(CAuthor author : mAuthors) {
//            int articlesNumber = mRandom.nextInt(maxArticlesPerAuthor + 1) + minArticlesPerAuthor;
//            for(int i = 0; i < articlesNumber - author.getArticles().size(); i++) {
//                CArticle article = createArticle();
//                mArticles.add(article);
//                author.addArticle(article);
//
//                int coauthorsNumber = mRandom.nextInt(maxCoauthors + 1) + minCoauthors;
//                for(int j = 0; j < coauthorsNumber; j++) {
//                    int coauthor = mRandom.nextInt(mAuthors.length);
//                    if(mAuthors[coauthor].getArticles().size() >= maxArticlesPerAuthor) {
//                        continue;
//                    }
//                    mAuthors[coauthor].addArticle(article);
//                }
//            }
//        }
    }

    private CArticle createArticle() {
//        int minKeyWordsPerArticle = getChildAttrValue("keyWordsPerArticle", "min");
//        int maxKeyWordsPerArticle = getChildAttrValue("keyWordsPerArticle", "max");
//        int minLinksPerArticle = getChildAttrValue("linksPerArticle", "min");
//        int maxLinksPerArticle = getChildAttrValue("linksPerArticle", "max");
//
//        CArticle article = new CArticle();
//
//        int keyWordsNumber = mRandom.nextInt(maxKeyWordsPerArticle + 1) + minKeyWordsPerArticle;
//        for(int i = 0; i < keyWordsNumber; i++) {
//            int keyWord = mRandom.nextInt(mKeyWords.length);
//            article.addKeyWord(mKeyWords[keyWord]);
//        }
//
//        int linksNumber = Math.min(mArticles.size(), mRandom.nextInt(maxLinksPerArticle + 1) + minLinksPerArticle);
//        for(int i = 0; i < linksNumber; i++) {
//            int link = mRandom.nextInt(mArticles.size());
//            article.addLink(mArticles.get(link));
//        }
//
//        return article;
        return null;
    }

    private int getChildAttrValue(String node, String attr) {
        return Integer.parseInt(
                mConfig.getElementsByTagName(node).item(0).getAttributes().getNamedItem(attr).getNodeValue()
        );
    }

    private void saveToDB() throws SQLException {
        Statement statement = mDBConnection.createStatement();

        for(CAuthor author : mAuthors) {
            author.saveToAuthor(statement);
        }

        for(CArticle article : mArticles) {
            article.saveToArticle(statement);
        }

        for(CKeyWord keyWord : mKeyWords) {
            keyWord.saveToKeyWord(statement);
        }

        for(CAuthor author : mAuthors) {
            author.saveToAuthorArticles(statement);
        }

        for(CArticle article : mArticles) {
            article.saveToArticleKeyWords(statement);
            article.saveToArticleLinks(statement);
        }

        statement.executeBatch();
        mDBConnection.commit();
        statement.close();
    }

    private void createSchema() throws SQLException {
        Statement statement = mDBConnection.createStatement();

        statement.addBatch(
                "CREATE TABLE author (\n" +
                "    id INT UNIQUE PRIMARY KEY NOT NULL,\n" +
                "    name VARCHAR(256) NOT NULL\n" +
                ");"
        );

        statement.addBatch(
                "CREATE TABLE article (\n" +
                "    id INT UNIQUE PRIMARY KEY NOT NULL,\n" +
                "    name VARCHAR(256) NOT NULL\n" +
                ");"
        );

        statement.addBatch(
                "CREATE TABLE key_word (\n" +
                "    id INT UNIQUE PRIMARY KEY NOT NULL,\n" +
                "    word VARCHAR(256) NOT NULL\n" +
                ");"
        );

        statement.addBatch(
                "CREATE TABLE author_articles (\n" +
                "    author_id INT NOT NULL,\n" +
                "    article_id INT NOT NULL,\n" +
                "    FOREIGN KEY (author_id) REFERENCES author(id),\n" +
                "    FOREIGN KEY (article_id) REFERENCES article(id)\n" +
                ");"
        );

        statement.addBatch(
                "CREATE TABLE article_key_words (\n" +
                "    article_id INT NOT NULL,\n" +
                "    key_word_id INT NOT NULL,\n" +
                "    FOREIGN KEY (key_word_id) REFERENCES key_word(id),\n" +
                "    FOREIGN KEY (article_id) REFERENCES article(id)\n" +
                ");"
        );

        statement.addBatch(
                "CREATE TABLE article_links (\n" +
                "    source_article_id INT NOT NULL,\n" +
                "    target_article_id INT NOT NULL,\n" +
                "    FOREIGN KEY (source_article_id) REFERENCES article(id),\n" +
                "    FOREIGN KEY (source_article_id) REFERENCES article(id)\n" +
                ");"
        );

        statement.addBatch(
                "CREATE VIEW author_articles_view AS \n" +
                "SELECT A.id, A.name, AA.article_id\n" +
                "FROM author A JOIN author_articles AA ON A.id == AA.author_id;"
        );

        statement.executeBatch();
        mDBConnection.commit();
        statement.close();
    }
}
