package com.scientists.graph.db.generator;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by vhapiak on 29.02.2016.
 */
public class CKeyWord {

    private static int ID = 0;

    private int mId;
    private String mWord;

    public CKeyWord() {
        this("Key word " + ID);
    }

    public CKeyWord(String word) {
        mId = ID++;
        mWord = word;
    }

    public void saveToKeyWord(Statement statement) throws SQLException {
        statement.addBatch(String.format(
                "INSERT INTO key_word \n" +
                "VALUES (%d, '%s');", mId, mWord));
    }

    public int getId() {
        return mId;
    }
}
