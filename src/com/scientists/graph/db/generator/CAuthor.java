package com.scientists.graph.db.generator;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by vhapiak on 29.02.2016.
 */
public class CAuthor {

    private static int ID = 0;

    private int mId;
    private String mName;
    private HashSet<CArticle> mArticles;

    public CAuthor() {
        this("Author" + ID);
    }

    public CAuthor(String name) {
        mId = ID++;
        mName = name;
        mArticles = new HashSet<>();
    }

    public Set<CArticle> getArticles() {
        return mArticles;
    }

    public void addArticle(CArticle article) {
        mArticles.add(article);
    }

    public void saveToAuthor(Statement statement) throws SQLException {
        statement.addBatch(String.format(
                "INSERT INTO author \n" +
                "VALUES (%d, '%s');", mId, mName));
    }

    public void saveToAuthorArticles(Statement statement) throws SQLException {
        for(CArticle article : mArticles) {
            statement.addBatch(String.format(
                    "INSERT INTO author_articles \n" +
                    "VALUES (%d, %d);", mId, article.getId()));
        }
    }

    public String getName() {
        return mName;
    }
}
