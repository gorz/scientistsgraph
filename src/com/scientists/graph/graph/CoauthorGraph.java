package com.scientists.graph.graph;

import com.scientists.graph.algorithm.Cluster;
import com.scientists.graph.data.Article;
import com.scientists.graph.data.Author;
import com.scientists.graph.data.DataManager;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Camera;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 13.11.2016.
 */
public class CoauthorGraph extends GraphBase {

    HashMap<String, Group<Author>> mGroups = new HashMap<>();
    List<Group<Author>> mGroupsList = new LinkedList<>();

    public CoauthorGraph(DataManager data)
    {
        super(new SingleGraph("Coauthors"), data);
        mGraph.setStrict(false);
        mGraph.setAutoCreate(true);

        HashMap<String, Author> authors = data.getAuthors();
        for (HashMap.Entry<String, Author> e : authors.entrySet())
        {
            Author author = e.getValue();
            Node vNode = mGraph.addNode(author.getName());
            vNode.addAttribute("ui.label", author.getName());
            for (Article article : author.getArticles())
            {
                for (Author a : article.getAuthors())
                {
                    if (author == a) continue;

                    Node uNode = mGraph.addNode(a.getName());
                    uNode.addAttribute("ui.label", a.getName());
                    Edge edge = uNode.getEdgeBetween(vNode);
                    if (edge == null)
                    {
                        edge = mGraph.addEdge(author.getName() + "_" + a.getName(), uNode, vNode);
                    }
                    Integer weight = edge.getAttribute("weight");
                    int w = weight == null ? 0 : weight;
                    w++;
                    edge.setAttribute("weight", w);
                    edge.setAttribute("ui.label", w);
                }
            }
        }

        for(Edge e : mGraph.getEachEdge())
        {
            int weight = e.getAttribute("weight");
            weight /= 2;
            e.setAttribute("weight", weight);
            e.setAttribute("ui.label", weight);
        }
    }

    @Override
    public void addCluster(Cluster cluster)
    {
        Group<Author> group = new Group<>();
        for (Node node : cluster)
        {
            Author author = mData.getAuthors().get(node.getAttribute("ui.label"));
            group.addItem(author);
            mGroups.put(author.getName(), group);
        }
        mGroupsList.add(group);
    }

    public Group<Author> getGroup(String name)
    {
        return mGroups.get(name);
    }

    public int getEdgeWeight(Author author1, Author author2)
    {
        Node node1 = mGraph.getNode(author1.getName());
        Node node2 = mGraph.getNode(author2.getName());
        Edge edge = node1.getEdgeBetween(node2);
        return edge == null ? 0 : edge.getAttribute("weight");
    }

    public List<Group<Author>> getGroups()
    {
        return mGroupsList;
    }
}
