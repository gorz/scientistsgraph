package com.scientists.graph.graph;

import com.scientists.graph.algorithm.Cluster;
import com.scientists.graph.data.Article;
import com.scientists.graph.data.DataManager;
import com.scientists.graph.data.KeyWord;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Camera;
import org.graphstream.ui.view.Viewer;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 13.11.2016.
 */
public class SemanticGraph extends GraphBase {

    private HashMap<String, Group<KeyWord>> mGroups = new HashMap<>();
    private List<Group<KeyWord>> mGroupsList = new LinkedList<>();

    public SemanticGraph(DataManager data)
    {
        super(new SingleGraph("Semantic"), data);
        mGraph.setStrict(false);
        mGraph.setAutoCreate(true);

        HashMap<String, KeyWord> kwyWords = data.getKeyWords();
        for (HashMap.Entry<String, KeyWord> e : kwyWords.entrySet())
        {
            KeyWord word = e.getValue();
            Node vNode = mGraph.addNode(word.getWord());
            vNode.addAttribute("ui.label", word.getWord());
            for (Article article : word.getArticles())
            {
                for (KeyWord k : article.getKeyWords())
                {
                    if (word == k) continue;

                    Node uNode = mGraph.addNode(k.getWord());
                    uNode.addAttribute("ui.label", k.getWord());
                    Edge edge = uNode.getEdgeBetween(vNode);
                    if (edge == null)
                    {
                        edge = mGraph.addEdge(word.getWord() + "_" + k.getWord(), uNode, vNode);
                    }
                    Integer weight = edge.getAttribute("weight");
                    int w = weight == null ? 0 : weight;
                    w++;
                    edge.setAttribute("weight", w);
                    edge.setAttribute("ui.label", w);
                }
            }
        }

        for(Edge e : mGraph.getEachEdge())
        {
            int weight = e.getAttribute("weight");
            weight /= 2;
            e.setAttribute("weight", weight);
            e.setAttribute("ui.label", weight);
        }

//        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
//        final Viewer v = mGraph.display();
//        v.getDefaultView().addMouseWheelListener(new MouseWheelListener() {
//            @Override
//            public void mouseWheelMoved(MouseWheelEvent e) {
//                Camera c = v.getDefaultView().getCamera();
//                c.setViewPercent(c.getViewPercent() + e.getPreciseWheelRotation() / 100);
//                c.setViewCenter(-(e.getComponent().getWidth() / 2 - e.getX()) / 100 , (e.getComponent().getHeight() / 2 - e.getY()) / 100, c.getViewCenter().z);
//            }
//        });

    }

    @Override
    public void addCluster(Cluster cluster)
    {
        Group<KeyWord> group = new Group<>();
        for (Node node : cluster)
        {
            KeyWord word = mData.getKeyWords().get(node.getAttribute("ui.label"));
            group.addItem(word);
            mGroups.put(word.getWord(), group);
        }
        mGroupsList.add(group);
    }

    public Group<KeyWord> getGroup(String word)
    {
        return mGroups.get(word);
    }

    public List<Group<KeyWord>> getGroups()
    {
        return mGroupsList;
    }
}
