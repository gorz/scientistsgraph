package com.scientists.graph.graph;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 14.11.2016.
 */
public class Group<T> implements Iterable<T> {

    private List<T> mItems = new LinkedList<T>();

    void addItem(T item) {
        mItems.add(item);
    }

    @Override
    public Iterator<T> iterator() {
        return mItems.iterator();
    }
}
