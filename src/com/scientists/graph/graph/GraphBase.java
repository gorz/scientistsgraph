package com.scientists.graph.graph;

import com.scientists.graph.algorithm.Cluster;
import com.scientists.graph.data.DataManager;
import org.graphstream.graph.Graph;

/**
 * Created by vhapiak on 14.11.2016.
 */
public abstract class GraphBase {

    protected Graph mGraph;
    protected DataManager mData;

    public GraphBase(Graph graph, DataManager data)
    {
        mGraph = graph;
        mData = data;
    }

    public Graph getGraph()
    {
        return mGraph;
    }

    public abstract void addCluster(Cluster cluster);

}
