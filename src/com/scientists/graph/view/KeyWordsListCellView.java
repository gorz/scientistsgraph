package com.scientists.graph.view;

import com.scientists.graph.controller.AuthorsCell;
import com.scientists.graph.controller.KeyWordsCell;
import com.scientists.graph.data.Author;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.Group;
import javafx.scene.control.ListCell;

import java.io.IOException;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class KeyWordsListCellView extends ListCell<Group<KeyWord>> {

    @Override
    protected void updateItem(Group<KeyWord> item, boolean empty) {
        if (item == null || empty) {
            setGraphic(null);
        }
        else
        {
            try {
                KeyWordsCell cell = new KeyWordsCell(item);
                setGraphic(cell.getPane());
            } catch (IOException e) {
                throw new RuntimeException("Cannot parse fxml", e);
            }
        }
    }

}
