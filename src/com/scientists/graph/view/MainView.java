package com.scientists.graph.view;

import com.scientists.graph.Model;
import com.scientists.graph.controller.Controller;
import com.scientists.graph.controller.Menu;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class MainView extends Application {

    private static String sFilename;
    private static boolean hasFile;

    public static void main(String[] args) {
        hasFile = false;
        if (args.length >= 1)
        {
            hasFile = true;
            sFilename = args[0];
        }
        launch(args);
    }

    private Model mModel;

    @Override
    public void start(Stage primaryStage) {
        try {
            if (hasFile)
                mModel = new Model(sFilename);
            else
                mModel = new Model();
        } catch (SQLException e) {
            throw new RuntimeException("Wrong file", e);
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource("menu.fxml"));
        try {
            Parent root = loader.load();
            Controller controller = loader.getController();
            controller.init(primaryStage, mModel);
            primaryStage.setTitle("Система аналізу публікацій");
            primaryStage.setScene(new Scene(root, 350, 265));
            primaryStage.show();
        } catch (IOException e) {
            throw new RuntimeException("Cannot parse fxml", e);
        }
    }
}
