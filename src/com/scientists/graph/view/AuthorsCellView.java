package com.scientists.graph.view;

import com.scientists.graph.controller.AuthorsCell;
import com.scientists.graph.data.Author;
import com.scientists.graph.graph.Group;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class AuthorsCellView extends ListCell<Group<Author>> {

    @Override
    protected void updateItem(Group<Author> item, boolean empty) {
        if (empty) {
            setGraphic(null);
        }
        else
        {
            try {
                AuthorsCell cell = new AuthorsCell(item);
                setGraphic(cell.getPane());
            } catch (IOException e) {
                throw new RuntimeException("Cannot parse fxml", e);
            }
        }
    }

}
