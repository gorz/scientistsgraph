package com.scientists.graph.view;

import com.scientists.graph.controller.Directions;
import com.scientists.graph.controller.KeyWordsCell;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.Group;
import javafx.geometry.Insets;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableCell;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;

import java.io.IOException;

/**
 * Created by vhapiak on 03.12.2016.
 */
public class KeyWordsTableCellView extends TableCell<Directions.Result, Group<KeyWord>> {

    @Override
    protected void updateItem(Group<KeyWord> item, boolean empty) {
        super.updateItem(item, empty);
        if (!empty && item != null)
        {
            try {
                KeyWordsCell cell = new KeyWordsCell(item);
                FlowPane pane = cell.getPane();
                pane.autosize();
                setPrefHeight(pane.getHeight());
                setGraphic(pane);
            } catch (IOException e) {
                throw new RuntimeException("Cannot parse fxml", e);
            }
        }
        else
        {
            setGraphic(null);
        }
    }

}
