package com.scientists.graph;

import com.scientists.graph.algorithm.Algorithm;
import com.scientists.graph.algorithm.ConnectedComponents;
import com.scientists.graph.algorithm.MCL;
import com.scientists.graph.algorithm.SpanningTree;
import com.scientists.graph.controller.NewRelations;
import com.scientists.graph.data.Author;
import com.scientists.graph.data.DataManager;
import com.scientists.graph.data.KeyWord;
import com.scientists.graph.graph.CoauthorGraph;
import com.scientists.graph.graph.Group;
import com.scientists.graph.graph.SemanticGraph;
import com.scientists.graph.search.AuthorsSearch;
import com.scientists.graph.search.CoauthorSearch;
import com.scientists.graph.search.DirectionsSearch;
import com.scientists.graph.search.NewRelationsSearch;
import org.graphstream.graph.Graph;
import scala.util.parsing.combinator.testing.Str;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vhapiak on 02.03.2016.
 */
public class Model {

    static {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private DataManager mData;
    private SemanticGraph mSemanticGraph;
    private CoauthorGraph mCoauthorGraph;
    private Algorithm mCoauthorAlgorithm;
    private Algorithm mSemanticAlgorithm;

    private CoauthorSearch mCoathursSearch;
    private DirectionsSearch mDirectionsSearch;
    private AuthorsSearch mAuthorsSearch;
    private NewRelationsSearch mNewRelationsSearch;

    public Model()
    {
        mCoauthorAlgorithm = new MCL(2);
        mSemanticAlgorithm = new MCL(1, 1);

        mData = new DataManager();
        mCoauthorGraph = new CoauthorGraph(mData);
        mSemanticGraph = new SemanticGraph(mData);

        mNewRelationsSearch = new NewRelationsSearch(mData, mSemanticGraph);
        mCoathursSearch = new CoauthorSearch(mData, mCoauthorGraph);
        mDirectionsSearch = new DirectionsSearch(mData, mSemanticGraph);
        mAuthorsSearch = new AuthorsSearch(mData, mSemanticGraph);
    }

    public Model(String file) throws SQLException {
        this();
        selectFile(file);
    }

    public List<Author> searchNewRelations(String author)
    {
        return mNewRelationsSearch.search(author);
    }

    public List<CoauthorSearch.Result> searchCoauthors(String author)
    {
        return mCoathursSearch.search(author);
    }

    public List<DirectionsSearch.Result> searchDirections(String author)
    {
        return mDirectionsSearch.search(author);
    }

    public List<AuthorsSearch.Result> searchAuthors(List<String> words)
    {
        return mAuthorsSearch.search(words);
    }

    public void selectFile(String path) throws SQLException {
        mData = new DataManager(path);
        mCoauthorGraph = new CoauthorGraph(mData);
        mSemanticGraph = new SemanticGraph(mData);

        long old = System.currentTimeMillis();
        mCoauthorAlgorithm.clusterize(mCoauthorGraph);
        long cur = System.currentTimeMillis();
        System.out.println("Coauthors: " + mData.getAuthors().size() + " " + (cur - old) + "ms");
        old = System.currentTimeMillis();
        mSemanticAlgorithm.clusterize(mSemanticGraph);
        cur = System.currentTimeMillis();
        System.out.println("Semantic: " + mData.getKeyWords().size() + " " + (cur - old) + "ms");

        mNewRelationsSearch = new NewRelationsSearch(mData, mSemanticGraph);
        mCoathursSearch = new CoauthorSearch(mData, mCoauthorGraph);
        mDirectionsSearch = new DirectionsSearch(mData, mSemanticGraph);
        mAuthorsSearch = new AuthorsSearch(mData, mSemanticGraph);
    }


    public List<Group<Author>> getAuthorsGroups()
    {
        return mCoauthorGraph.getGroups();
    }

    public List<Group<KeyWord>> getKeyWordsGroups()
    {
        return mSemanticGraph.getGroups();
    }

    public DataManager getData()
    {
        return mData;
    }
//    public static void main(String[] args) throws SQLException {
//        System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
//        if(args.length < 1) {
//            System.out.println("Args: db.db");
//            return;
//        }
//
//        DataManager data = new DataManager(args[0]);
//        CoauthorGraph g = new CoauthorGraph(data);
//        SemanticGraph g = new SemanticGraph(data);
//        Graph graph = g.getGraph();
//
//        System.out.print("Nodes: " + graph.getNodeCount());

//        ConnectedComponents algo = new ConnectedComponents(3);
//        SpanningTree algo = new SpanningTree(3);
//        MCL algo = new MCL(2);
//        algo.clusterize(g);

//        CoauthorSearch search = new CoauthorSearch(data, g);
//        List<CoauthorSearch.Result> results = search.search("Крисилов");
//        DirectionsSearch search = new DirectionsSearch(data, g);
//        List<DirectionsSearch.Result> results = search.search("Тройнина");
//        AuthorsSearch search = new AuthorsSearch(data, g);
//        List<String> searchData = new LinkedList<>();
//        searchData.add("AI");
//        List<AuthorsSearch.Result> results = search.search(searchData);
//        NewRelationsSearch search = new NewRelationsSearch(data, g);
//        List<Author> result = search.search("Крисилов");
//        return;
//    }
}
