CREATE TABLE author (
    id INT UNIQUE PRIMARY KEY NOT NULL,
    name VARCHAR(256) NOT NULL
);

CREATE TABLE article (
    id INT UNIQUE PRIMARY KEY NOT NULL,
    name VARCHAR(256) NOT NULL
);

CREATE TABLE key_word (
    id INT UNIQUE PRIMARY KEY NOT NULL,
    word VARCHAR(256) NOT NULL
);

CREATE TABLE author_articles (
    author_id INT NOT NULL,
    article_id INT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES author(id),
    FOREIGN KEY (article_id) REFERENCES article(id)
);

CREATE TABLE article_key_words (
    article_id INT NOT NULL,
    key_word_id INT NOT NULL,
    FOREIGN KEY (key_word_id) REFERENCES key_word(id),
    FOREIGN KEY (article_id) REFERENCES article(id)
);

CREATE TABLE article_links (
    source_article_id INT NOT NULL,
    target_article_id INT NOT NULL,
    FOREIGN KEY (source_article_id) REFERENCES article(id),
    FOREIGN KEY (source_article_id) REFERENCES article(id)
);